/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.agendacontactos;

import java.awt.BorderLayout;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

/**
 *
 * @author Victor
 */
public class ConsultaPersonas {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        EntityManagerFactory emf = Persistence
                .createEntityManagerFactory("AgendaContactosPU");
        EntityManager em = emf.createEntityManager();
        // TODO code application logic here

        System.out.println("Consulta 1 ==>");
        System.out.println("--------------");
        Query queryTotalHijos = em.createNativeQuery("SELECT SUM(NUM_HIJOS) FROM PERSONA");
        Integer sumaHijos = (Integer) queryTotalHijos.getSingleResult();
        System.out.println("El Numero total de hijos de todos los registros es: " + sumaHijos);

        System.out.println("--------------");
        System.out.println("Consulta 2 ==>");
        System.out.println("--------------");
        Query queryMaxSalario = em.createNativeQuery("SELECT MAX(SALARIO) FROM PERSONA");
        double maxSalario = (double) queryMaxSalario.getFirstResult();
        System.out.println("El mejor salario que hay es: " + maxSalario);

        System.out.println("--------------");
        System.out.println("Consulta 3 ==>");
        System.out.println("--------------");
        Query querymaxHijos = em.createNativeQuery("SELECT MAX(NUM_HIJOS) FROM PERSONA");
        Integer maxHijos = (Integer) querymaxHijos.getSingleResult();
        System.out.println("El Registro con el mayor numero de hijos es: " + maxHijos);

        System.out.println("--------------");
        System.out.println("Consulta 4 ==>");
        System.out.println("--------------");
        Query queryCuentaRegistros = em.createNativeQuery("SELECT COUNT(ID) FROM PERSONA");
        Integer cuentaRegistros = (Integer) queryCuentaRegistros.getSingleResult();
        System.out.println("Tienes un total de " + cuentaRegistros + " registros en la base de datos.");

      /*  System.out.println("--------------");
        System.out.println("Consulta 5 ==>");
        System.out.println("--------------");
        Query queryNacimiento = em.createNativeQuery("SELECT COUNT(FECHA_NACIMIENTO) FROM PERSONA WHERE FECHA_NACIMIENTO > 1995");
        double nacimiento = (double) queryNacimiento.getSingleResult();
        System.out.println(nacimiento);*/

    }
}
