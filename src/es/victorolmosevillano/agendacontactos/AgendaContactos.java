/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.victorolmosevillano.agendacontactos;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Victor
 */
public class AgendaContactos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //conectar con la base de datos.
        Map<String, String> emfProperties = new HashMap<String, String>();
        // emfProperties.put("javax.persistence.jdbc.user", "root");
        // emfProperties.put("javax.persistence.jdbc.password", "root");
        emfProperties.put("javax.persistence.schema-generation.database.action", "create");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("AgendaContactosPU", emfProperties);
        EntityManager em = emf.createEntityManager();
        emfProperties.put("javax.persistence.jdbc.url", "jdbc:derby:BDAgendaContactos;create=true");

        Provincia provinciaCadiz = new Provincia(0, "Cádiz");
        Provincia provinciaMalaga = new Provincia(0, "Malaga");
        Provincia provinciaSevilla = new Provincia();
        provinciaSevilla.setNombre("Sevilla");
        //transacciones.
        /*Se puede obtener iniciar una transacción sobre dicho EntityManager 
         con una sentencia como esta:*/
        //para iniciar las transacciones.
        em.getTransaction().begin();
        // Añadir aquí las operaciones de modificación de la base de datos
        em.persist(provinciaCadiz);
        em.persist(provinciaSevilla);

        em.persist(provinciaMalaga);

        //Realiza volcado definitivo a la base de datos (guarda las transacciones realizadas).
        em.getTransaction().commit();

        /*indicar la orden rollback que cancelará todas las operaciones pendientes 
         de volcar a la base de datos.
         esta acción sólo tendrá efecto si no se ha realizado un commit 
         anteriormente sobre los datos pendientes de volcar a la base de datos.*/
        //em.getTransaction().rollback();
        //cierra la conexion con la base de datos.
        em.close();
        emf.close();
        try {
            DriverManager.getConnection("jdbc:derby:BDAgendaContactos;shutdown=true");
        } catch (SQLException ex) {
        }
    }

}
